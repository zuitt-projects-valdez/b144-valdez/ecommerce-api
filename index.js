//dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//routes
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");

//server set up
const app = express();
const port = 4000;
// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//base uri
app.use("/users", userRoutes, orderRoutes);
app.use("/products", productRoutes);
//app.use("/orders", orderRoutes);

//database connection
mongoose.connect(
  "mongodb+srv://vvaldez626:admin@wdc028-course-booking.0r6u7.mongodb.net/ecommerce_api?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console.error, "connection error"));
db.once("open", () => console.log(`Now connected to MongoDB Atlas`));

//listen
app.listen(process.env.PORT || port, () =>
  console.log(`API is now online at port ${process.env.PORT || port}`)
);

//process.env.PORT - alternative port, normal port is for the local environment, this alternative is for the deployment environment
