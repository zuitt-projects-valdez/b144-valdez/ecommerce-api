const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  orders: [
    {
      productId: {
        type: String,
        required: true,
      },
      productName: {
        type: String,
        required: true,
      },
      numberOfItems: {
        type: Number,
        required: true,
      },
      price: {
        type: Number,
        required: true,
      },
      orderId: {
        type: String,
        default: "",
      },
      orderedOn: {
        type: Date,
        default: new Date(),
      },
      orderStatus: {
        type: String,
        default: "Pending",
      },
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
