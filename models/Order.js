const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  // orders: [
  // {
  // _id: String,
  userId: {
    type: String,
    required: true,
  },
  productId: [
    {
      type: String,
      required: true,
    },
  ],
  productName: [
    {
      type: String,
      required: true,
    },
  ],
  numberOfItems: [
    {
      type: Number,
      required: true,
    },
  ],
  totalAmount: {
    type: Number,
    required: true,
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
  orderStatus: {
    type: String,
    default: "Pending",
  },
  //   },
  // ],
});

module.exports = mongoose.model("Order", orderSchema);
