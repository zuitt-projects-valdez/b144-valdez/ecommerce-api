const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//register a user
module.exports.registerUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result !== null) {
      return false;
    } else {
      let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        isAdmin: reqBody.isAdmin,
      });
      return newUser.save().then((user, error) => {
        if (error) {
          return error;
        } else {
          return true;
        }
      });
    }
  });
};

//authenticate a user
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      if (isPasswordCorrect) {
        return { accessToken: auth.createAccessToken(result.toObject()) };
      } else {
        return false;
      }
    }
  });
};

//get user details
module.exports.getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    result.password = "";
    return result;
  });
};

//get pending cart items
// module.exports.getCartItems = (data) => {
//   let cartLength = 0;
//   let pendingOrders = [];
//   return User.findById(data.userId).then((user) => {
//     cartLength = user.orders.length;
//     for (let i = 0; i < cartLength; i++) {
//       if (user.orders[i].orderStatus === "Pending") {
//         pendingOrders.push({
//           productId: user.orders[i].productId,
//           productName: user.orders[i].productName,
//           numberOfItems: user.orders[i].numberOfItems,
//           price: user.orders[i].price,
//           orderId: user.orders[i].orderId
//         })
//         return pendingOrders
//       }
//     }
//   });
// };

//set a user as admin with an admin user
module.exports.setAsAdmin = (reqParams, userData) => {
  let updatedAdminStatus = {
    isAdmin: true,
  };
  return User.findById(userData.userId).then((result) => {
    if (userData.isAdmin == false) {
      return "You are not authorized to make this request";
    } else {
      return User.findByIdAndUpdate(reqParams.id, updatedAdminStatus).then(
        (user, error) => {
          if (error) {
            return false;
          } else {
            return `User ${user.firstName} ${user.lastName} is now an admin`;
          }
        }
      );
    }
  });
};

//get all users
module.exports.getAllUsers = () => {
  return User.find({}).then((result) => {
    return result;
  });
};

module.exports.addToCart = async (data) => {
  let productInfo = await Product.findById(data.productId).then((info, err) => {
    if (err) {
      return false;
    } else {
      return info;
    }
  });
  if (productInfo.isActive !== false) {
    return User.findById(data.userId).then((user) => {
      user.orders.push({
        productId: data.productId,
        productName: productInfo.name,
        numberOfItems: data.numberOfItems,
        price: productInfo.price,
      });
      return user.save().then((result, error) => {
        if (error) {
          console.log(error);
        } else {
          return true;
        }
      });
    });
  }
};

module.exports.changeQuantity = (userId, orders) => {
  return User.findById(userId).then((result, error) => {
    if (error) {
      return false;
    } else {
      result.orders[orders.index].numberOfItems = orders.numberOfItems;
      return result.save().then((res, err) => {
        if (err) {
          return false;
        } else {
          return true;
        }
      });
    }
  });
};

module.exports.removeProduct = (userId, cartIndex) => {
  return User.findById(userId).then((user) => {
    user.orders.splice(cartIndex, 1);
    return user.save().then((result, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  });
};

module.exports.checkout = async (userId) => {
  let total = 0;
  let purchase = [];
  let quantity = [];
  let productNames = [];
  // let price = [];
  let i = 0;
  let cartItems = 0;
  let createdOrder = 0;
  let completedStatus = "Completed";
  // let newOrder = new Order();

  let isOrderCreated = await User.findById(userId).then((user, err) => {
    if (err) {
      return false;
    } else if (user.orders.length == 0) {
      //empty cart
      return false;
    } else {
      cartItems = user.orders.length;
      for (i = 0; i < cartItems; i++) {
        console.log(user.orders[i].numberOfItems);
        console.log(user.orders[i].price);
        total =
          Number(total) +
          Number(user.orders[i].numberOfItems) * Number(user.orders[i].price);
        productNames.push(user.orders[i].productName);
        purchase.push(user.orders[i].productId);
        quantity.push(user.orders[i].numberOfItems);
        // price.push(user.orders[i].price);
      }
      let newOrder = new Order({
        userId: userId,
        productId: purchase,
        numberOfItems: quantity,
        // price: price,
        totalAmount: total,
        orderStatus: completedStatus,
      });
      // console.log(total);
      // newOrder.orders.push({
      //   userId: userId,
      //   productId: purchase,
      //   numberOfItems: quantity,
      //   totalAmount: total,
      // });
      return newOrder.save().then((order, err) => {
        if (err) {
          return false;
        } else {
          // createdOrder = order.id;
          return true;
        }
      });
    }
  });
  //change order

  let isUserUpdated = await User.findById(userId).then((user, err) => {
    if (err) {
      return false;
    } else {
      // for (i = 0; i < cartItems; i++) {
      //   user.orders[i].orderId = createdOrder;
      //   user.orders[i].orderStatus = completedStatus;
      //   user.save((res, err) => {
      //     if (err) {
      //       return false;
      //     }
      //   });
      // }
      user.orders.length = 0;
      user.save((res, err) => {
        if (err) {
          return false;
        } else {
          return true;
        }
      });
    }
  });

  if (isOrderCreated && isUserUpdated) {
    return true;
  } else {
    return true;
  }
};
