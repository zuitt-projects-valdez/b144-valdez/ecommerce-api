const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");

//create order
// module.exports.createOrder = async (data, userData) => {
//   let productPrice = await Product.findById(data.productId).then((price) => {
//     return price.price;
//   });

//   let productName = await Product.findById(data.productId).then((name) => {
//     return name.name;
//   });

//   let newOrder = new Order();
//   let productData = data.productId;
//   let numberOfItems = Number(data.amount); //change to number data type
//   let orderStatus = "Completed";

//   let isOrderUpdated = await Order.findById(userData.userId).then((order) => {
//     if (userData.isAdmin == true) {
//       return false;
//     } else {
//       newOrder.orders.push({
//         userId: userData.userId,
//         productId: productData,
//         productName: productName,
//         numberOfItems: numberOfItems,
//         totalAmount: numberOfItems * productPrice,
//         orderStatus: orderStatus,
//       });

//       return newOrder.save().then((order, error) => {
//         if (error) {
//           return false;
//         } else {
//           return true;
//         }
//       });
//     }
//   });

//   let isUserUpdated = await User.findById(userData.userId).then((user) => {
//     user.orders.push({
//       productId: data.productId,
//       productName: productName,
//       numberOfItems: numberOfItems,
//       totalAmount: numberOfItems * productPrice,
//     });
//     return user.save().then((newOrder, error) => {
//       if (error) {
//         return false;
//       } else {
//         return true;
//       }
//     });
//   });

//   if (isOrderUpdated == true && isUserUpdated == true) {
//     return true;
//   } else {
//     return false;
//   }
// };

//get all orders with admin authorization
module.exports.getAllOrders = (userData) => {
  if (userData.isAdmin == false) {
    return false;
  } else {
    return Order.find({}).then((result) => {
      return result;
    });
  }
};

//get authenticated user's orders
module.exports.getUserOrders = (userId) => {
  return Order.find({ userId: userId }).then((result, error) => {
    if (error) {
      return false;
    } else {
      return result;
    }
  });
};
