const Product = require("../models/Product");

//add a product
module.exports.addProduct = (reqBody, userData) => {
  return Product.findById(userData.userId).then((result) => {
    if (userData.isAdmin == false) {
      return false;
    } else {
      let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        isActive: reqBody.isActive,
      });

      return newProduct.save().then((product, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });
    }
  });
};

//get active products
module.exports.getActiveProducts = () => {
  return Product.find({ isActive: true }).then((result) => {
    return result;
  });
};

//get all products
module.exports.getAllProducts = () => {
  return Product.find({}).then((result) => {
    return result;
  });
};

//get specific product
module.exports.getOneProduct = (reqParams) => {
  return Product.findById(reqParams.productId).then((result) => {
    return result;
  });
};

//update product
module.exports.updateProduct = (reqParams, reqBody, userData) => {
  return Product.findById(userData.userId).then((result) => {
    if (userData.isAdmin) {
      let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
      };
      return Product.findByIdAndUpdate(
        reqParams.productId,
        updatedProduct
      ).then((updated, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });
    } else {
      return false;
    }
  });
};

//archive a product
module.exports.archiveProduct = (reqParams, userData) => {
  let archivedProduct = {
    isActive: false,
  };
  return Product.findById(userData.userId).then((result) => {
    if (userData.isAdmin == false) {
      return false;
    } else {
      return Product.findByIdAndUpdate(
        reqParams.productId,
        archivedProduct
      ).then((product, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });
    }
  });
};
module.exports.unarchiveProduct = (reqParams, userData) => {
  let unarchivedProduct = {
    isActive: true,
  };
  return Product.findById(userData.userId).then((result) => {
    if (userData.isAdmin == false) {
      return false;
    } else {
      return Product.findByIdAndUpdate(
        reqParams.productId,
        unarchivedProduct
      ).then((product, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });
    }
  });
};
