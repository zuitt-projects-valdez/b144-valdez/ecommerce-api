const express = require("express");
const router = express.Router();
const auth = require("../auth");

//controller
const productController = require("../controllers/product");

//create a product
router.post("/", auth.verify, (req, res) => {
  let userData = auth.decode(req.headers.authorization);
  productController
    .addProduct(req.body, {
      userId: userData.id,
      isAdmin: userData.isAdmin,
    })
    .then((result) => res.send(result));
});

//get all active products
router.get("/", (req, res) => {
  productController.getActiveProducts().then((result) => res.send(result));
});

//get all products
router.get("/all", (req, res) => {
  productController.getAllProducts().then((result) => res.send(result));
});

//get a single product
router.get("/:productId", (req, res) => {
  productController.getOneProduct(req.params).then((result) => {
    res.send(result);
  });
});

//update a product
router.put("/:productId", auth.verify, (req, res) => {
  let userData = auth.decode(req.headers.authorization);
  console.log(req.params);
  productController
    .updateProduct(req.params, req.body, {
      userId: userData.id,
      isAdmin: userData.isAdmin,
    })
    .then((result) => res.send(result));
});

//archive a product
router.put("/:productId/archive", auth.verify, (req, res) => {
  let userData = auth.decode(req.headers.authorization);
  productController
    .archiveProduct(req.params, {
      userId: userData.id,
      isAdmin: userData.isAdmin,
    })
    .then((result) => res.send(result));
});

router.put("/:productId/unarchive", auth.verify, (req, res) => {
  let userData = auth.decode(req.headers.authorization);
  productController
    .unarchiveProduct(req.params, {
      userId: userData.id,
      isAdmin: userData.isAdmin,
    })
    .then((result) => res.send(result));
});

module.exports = router;
