const express = require("express");
const router = express.Router();
const auth = require("../auth");

//controllers
const orderController = require("../controllers/order");

//create order
// router.post("/checkout", auth.verify, (req, res) => {
//   let userData = auth.decode(req.headers.authorization);
//   let data = [
//     {
//       productId: req.body.productId,
//       amount: req.body.amount,
//     },
//   ];
//   orderController
//     .createOrder(data, { userId: userData.id, isAdmin: userData.isAdmin })
//     .then((result) => res.send(result));
// });

//get all orders
router.get("/orders", auth.verify, (req, res) => {
  let userData = auth.decode(req.headers.authorization);
  orderController
    .getAllOrders({ userId: userData.id, isAdmin: userData.isAdmin })
    .then((result) => res.send(result));
});

//get authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {
  let userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    response.send("Feature disabled for admin users");
  } else {
    orderController
      .getUserOrders(userData.id)
      .then((result) => res.send(result));
  }
});
module.exports = router;
