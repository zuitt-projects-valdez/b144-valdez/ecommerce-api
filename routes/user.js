const express = require("express");
const router = express.Router();
const auth = require("../auth");

//Controllers
const userController = require("../controllers/user");

//User registration
router.post("/register", (req, res) => {
  userController.registerUser(req.body).then((result) => res.send(result));
});

//User authentication
router.post("/login", (req, res) => {
  userController.loginUser(req.body).then((result) => res.send(result));
});

//User details
router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  userController
    .getProfile({ userId: userData.id })
    .then((result) => res.send(result));
});
// router.get("/cartItems", auth.verify, (req, res) => {
//   const userData = auth.decode(req.headers.authorization);

//   userController
//     .getCartItems({ userId: userData.id })
//     .then((result) => res.send(result));
// });

//set user as admin
router.put("/:id/setAsAdmin", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .setAsAdmin(req.params, {
      userId: userData.id,
      isAdmin: userData.isAdmin,
    })
    .then((result) => res.send(result));
});

//get all users
router.get("/", (req, res) => {
  userController.getAllUsers().then((result) => res.send(result));
});

//add to cart
router.post("/addToCart", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  let data = {
    userId: userData.id,
    productId: req.body.productId,
    numberOfItems: req.body.numberOfItems,
  };
  userController.addToCart(data).then((result) => res.send(result));
});

//change quantity of items in cart
router.put("/changeQuantity", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .changeQuantity(userData.id, req.body)
    .then((result) => res.send(result));
});

//remove product from cart
router.put("/removeProduct", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .removeProduct(userData.id, req.body.index)
    .then((result) => res.send(result));
});

//place order
router.post("/checkout", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  const userId = userData.id;
  userController.checkout(userId).then((result) => res.send(result));
});

module.exports = router;
